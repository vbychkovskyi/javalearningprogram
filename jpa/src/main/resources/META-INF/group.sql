CREATE TABLE academic_groups (
  group_name VARCHAR(30) PRIMARY KEY,
  head       VARCHAR(30)
);

CREATE TABLE student (
  name           VARCHAR(30) PRIMARY KEY,
  age            INTEGER,
  academic_group VARCHAR(30),
  fk REFERENCES academic_groups (group_name)
);