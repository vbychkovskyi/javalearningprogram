package com.example.main;

import com.example.model.students.Student;
import com.example.service.Service;
import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Employee: Vyacheslav.Bychkovsk
 * Date: 17.06.13
 * Time: 16:40
 */
public class App {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();

        ctx.load("META-INF/spring/spring-config.xml");
        ctx.refresh();

        Service<Student> service = ctx.getBean(Service.class);

        Student student = new Student();
        student.setName("Joec");
        student.setAge(20);
        service.save(student);
    }
}
