package com.example.jpa.test;

import com.example.model.students.Group;
import com.example.model.students.Student;
import com.example.service.Service;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class DataSourceTest extends AbstractSpringTest {

    @Autowired
    private Service<Student> studentService;

    @Test
    public void saveTest() {

        String name = "XAa";
        int age = 20;

        Student student;

        student = new Student();
        student.setName(name);
        student.setAge(age);

        studentService.save(student);

        student = studentService.find(name);
        assertNotNull("After saving student must not be null", student);
        assertEquals("Received student name must be equal local student name", student.getName(), name);

    }

    @Test
    public void testOneToMany() throws Exception {

        String name = "Some name";
        String groupName = "Pi";
        int age = 20;

        Group group = new Group();
        group.setName(groupName);
        group.setHead("Head");

        Student student = new Student();

        student.setAge(age);
        student.setName(name);
        student.setGroup(group);

        studentService.save(student);
        student = studentService.find(name);
        assertNotNull("After saving the student must not be null", student);
        assertEquals("Received entity fields must be equal local fields", student.getName(), name);
        assertEquals("Received group must be equal the same local group", student.getGroup(), group);
    }
}
