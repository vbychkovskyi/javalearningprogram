package com.example.jpa.test;

import com.example.model.plants.classId.Key;
import com.example.model.plants.classId.TableEntity;
import com.example.model.plants.embedableId.CompositeKey;
import com.example.model.plants.embedableId.PreparationForPlantAndPest;
import com.example.service.Service;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class PlantsEntityTest extends AbstractSpringTest {

    @Autowired
    @Qualifier("preparationService")
    private Service<PreparationForPlantAndPest> pestService;

    @Qualifier("compositeKey")
    @Autowired
    private Service<TableEntity> tableEntityService;

    @Test
    public void testEmbedable() throws Exception {
        PreparationForPlantAndPest value = new PreparationForPlantAndPest();

        CompositeKey key = new CompositeKey();
        key.setPest("Xpobak");
        key.setPlant("Apple");

        value.setCompositeKey(key);
        value.setPreparation("HCl");

        pestService.save(value);
        value = pestService.find(key);
        assertNotNull("Data received from data base must not be null", value);
    }

    @Test
    public void testIdClass() throws Exception {
        TableEntity value = new TableEntity();

        String keyPest = "Xpobak";
        String keyOnion = "Onion";

        Key key = new Key(keyOnion, keyPest);

        value.setPest(keyPest);
        value.setPlant(keyOnion);

        value.setPreparation("HCl");

        tableEntityService.save(value);
        TableEntity value2 = tableEntityService.find(key);
        assertNotNull("Data received from data base must not be null", value);
        assertEquals("Old entity value must be equal received value", value, value2);
    }

    @Test
    public void testKeys() throws Exception {
        //Local test values
        String xpobak = "Xpobak";
        String preparation = "C2H5OH";
        String onion = "Onion";
        String apple = "Apple";

        Key key = new Key();
        key.setPest(xpobak);
        key.setPlant(onion);


        TableEntity entity = new TableEntity();
        entity.setPest(key.getPest());
        entity.setPlant(key.getPlant());
        entity.setPreparation(preparation);

        tableEntityService.save(entity);
        entity = tableEntityService.find(key);

        assertNotNull(entity);
        String message = "Received values must be equal local values";
        assertEquals(message, key.getPest(), entity.getPest());
        assertEquals(message, key.getPlant(), entity.getPlant());

        CompositeKey key2 = new CompositeKey();
        key2.setPest(xpobak);
        key2.setPlant(apple);

        PreparationForPlantAndPest target = new PreparationForPlantAndPest();
        target.setCompositeKey(key2);
        target.setPreparation(preparation);
        pestService.save(target);

        target = pestService.find(key2);

        assertNotNull("Saved value must not be null", target);
        assertEquals(message, target.getCompositeKey(), key2);

    }
}
