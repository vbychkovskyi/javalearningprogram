package com.example.jpa.test;

import com.example.model.jpql.Employee;
import com.example.model.jpql.EmployeeKey;
import com.example.service.Service;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class JPQLTest extends AbstractSpringTest {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private Service<Employee> employeeService;

    @Test
    public void testQueries() throws Exception {
        List<Employee> resultList = employeeService.findAll();
        assertNotNull(resultList);
    }

    @Test
    public void testCriteriaAPI() throws Exception {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> cq = builder.createQuery(Employee.class);
        Root<Employee> employee = cq.from(Employee.class);
        cq.select(employee);
        TypedQuery<Employee> q = entityManager.createQuery(cq);
        List<Employee> all = q.getResultList();

        assertNotNull("Data must not be null", all);
        assertEquals("Data returned by service and criteria api must be equal", all, employeeService.findAll());
    }

    @Test
    public void testEmployee() throws Exception {
        Employee employee;

        String firstName = "Vyacheslav";
        String lastName = "Bychkovsyy";

        EmployeeKey employeeKey = new EmployeeKey(firstName, lastName);

        employee = employeeService.find(employeeKey);
        assertNull(employee);

        String email = "vby@gmail.com";
        String phoneNumber = "124214";
        String appS = "AppS";

        employee = new Employee(firstName, lastName, email, phoneNumber, appS, new Date());
        employeeService.save(employee);

        employee = employeeService.find(employeeKey);

        assertNotNull(employee);
    }
}
