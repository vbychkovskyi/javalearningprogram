package com.example.model.inheritance;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 21.06.13
 * Time: 11:59
 */
@Entity
@Table(name = "T1")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name = "D", discriminatorType = DiscriminatorType.STRING, length = 20)
public class Entity1 {

    @Id
    @Column(name = "F1")
    private String field1;

    @Column(name = "F2")
    private String field2;

    public Entity1() {
    }

    public Entity1(String field1, String field2) {
        this.field1 = field1;
        this.field2 = field2;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entity1 entity1 = (Entity1) o;

        if (field1 != null ? !field1.equals(entity1.field1) : entity1.field1 != null) return false;
        if (field2 != null ? !field2.equals(entity1.field2) : entity1.field2 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = field1 != null ? field1.hashCode() : 0;
        result = 31 * result + (field2 != null ? field2.hashCode() : 0);
        return result;
    }
}
