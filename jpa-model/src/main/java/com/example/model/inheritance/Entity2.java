package com.example.model.inheritance;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 21.06.13
 * Time: 11:59
 */

@Entity
@Table(name = "T2")
@DiscriminatorValue(value = "E2")
public class Entity2 extends Entity1 {

    @Id
    @Column(name = "F3")
    private String field21;

    @Column(name = "F4")
    private String field22;

    public Entity2() {

    }

    public Entity2(String field21, String field22) {
        this.field21 = field21;
        this.field22 = field22;
    }

    public String getField21() {
        return field21;
    }

    public void setField21(String field21) {
        this.field21 = field21;
    }

    public String getField22() {
        return field22;
    }

    public void setField22(String field22) {
        this.field22 = field22;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Entity2 entity2 = (Entity2) o;

        if (field21 != null ? !field21.equals(entity2.field21) : entity2.field21 != null) return false;
        if (field22 != null ? !field22.equals(entity2.field22) : entity2.field22 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (field21 != null ? field21.hashCode() : 0);
        result = 31 * result + (field22 != null ? field22.hashCode() : 0);
        return result;
    }
}
