package com.example.model.inheritance;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 25.06.13
 * Time: 11:50
 */

@Entity
@Table(name = "T3")
@DiscriminatorValue("E3")
public class Entity3 extends Entity1 {

    @Column(name = "F5")
    private String value1;

    @Column(name = "F6")
    private String value2;

    public Entity3() {

    }

    public Entity3(String field1, String field2, String value1, String value2) {
        super(field1, field2);
        this.value1 = value1;
        this.value2 = value2;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }
}
