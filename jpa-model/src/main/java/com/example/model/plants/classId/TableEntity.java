package com.example.model.plants.classId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 20.06.13
 * Time: 17:08
 */
@Entity
@IdClass(Key.class)
@Table(name = "pest_avoidance")
public class TableEntity {
    @Id
    private String pest;

    @Id
    private String plant;

    private String preparation;

    public String getPest() {
        return pest;
    }

    public void setPest(String pest) {
        this.pest = pest;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }
}
