package com.example.model.plants.embedableId;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 20.06.13
 * Time: 15:20
 */
@Entity
@Table(name = "pest_avoidance")
public class PreparationForPlantAndPest implements Serializable {

    @EmbeddedId
    private CompositeKey compositeKey;

    @Column(name = "PREPARATION")
    private String preparation;

    public String getPreparation() {
        return preparation;
    }

    public CompositeKey getCompositeKey() {
        return compositeKey;
    }

    public void setCompositeKey(CompositeKey compositeKey) {
        this.compositeKey = compositeKey;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }
}
