package com.example.model.plants.classId;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 20.06.13
 * Time: 17:08
 */
public class Key implements Serializable {
    String plant;
    String pest;

    public Key() {
    }

    public Key(String plant, String pest) {
        this.plant = plant;
        this.pest = pest;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getPest() {
        return pest;
    }

    public void setPest(String pest) {
        this.pest = pest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Key key = (Key) o;

        if (!pest.equals(key.pest)) return false;
        if (!plant.equals(key.plant)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = plant.hashCode();
        result = 31 * result + pest.hashCode();
        return result;
    }
}
