package com.example.model.plants.embedableId;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 20.06.13
 * Time: 15:25
 */
@Embeddable
public class CompositeKey implements Serializable {

    @Column(name = "PLANT", nullable = false)
    private String plant;

    @Column(name = "PEST", nullable = false)
    private String pest;


    public CompositeKey() {

    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getPest() {
        return pest;
    }

    public void setPest(String pest) {
        this.pest = pest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CompositeKey that = (CompositeKey) o;

        if (!pest.equals(that.pest)) return false;
        if (!plant.equals(that.plant)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = plant.hashCode();
        result = 31 * result + pest.hashCode();
        return result;
    }
}
