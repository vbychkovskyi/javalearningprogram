package com.example.model.jpql;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 26.06.13
 * Time: 10:52
 */
public class EmployeeKey implements Serializable {
    private String firstName;
    private String lastName;

    public EmployeeKey() {

    }

    public EmployeeKey(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
