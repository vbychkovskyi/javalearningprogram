package com.example.model.students;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 17.06.13
 * Time: 15:58
 */
@Entity
@Table(name = "academic_groups")
public class Group implements Serializable {

    @Id
    @Column(name = "group_name")
    private String name;

    @Column(name = "head")
    private String head;

    @OneToMany(mappedBy = "group", cascade = {CascadeType.ALL})
    private List<Student> students;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<Student> getStudents() {
        return students;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (head != null ? !head.equals(group.head) : group.head != null) return false;
        if (name != null ? !name.equals(group.name) : group.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (head != null ? head.hashCode() : 0);
        return result;
    }
}
