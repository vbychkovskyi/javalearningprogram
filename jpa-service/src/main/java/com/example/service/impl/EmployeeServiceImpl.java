package com.example.service.impl;

import com.example.model.jpql.Employee;
import com.example.service.GenericDAO;
import com.example.service.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 11.07.13
 * Time: 15:39
 */
@Transactional
public class EmployeeServiceImpl implements Service<Employee> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Employee find(Object primaryKey) {
        return entityManager.find(Employee.class, primaryKey);
    }

    @Override
    public List<Employee> findAll() {
        Query q = entityManager.createQuery("select e from Employee e");
        return q.getResultList();
    }

    @Override
    public void save(Employee employee) {
        entityManager.persist(employee);
    }

    @Override
    public void update(Employee employee) {
        entityManager.merge(employee);
    }

    @Override
    public void delete(Employee employee) {
        entityManager.remove(entityManager.merge(employee));
    }

    @Override
    public void setDao(GenericDAO<Employee> dao) {

    }
}
