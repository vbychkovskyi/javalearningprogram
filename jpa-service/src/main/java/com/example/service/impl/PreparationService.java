package com.example.service.impl;

import com.example.model.plants.embedableId.PreparationForPlantAndPest;
import com.example.service.GenericDAO;
import com.example.service.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 20.06.13
 * Time: 15:58
 */
public class PreparationService implements Service<PreparationForPlantAndPest> {

    private GenericDAO<PreparationForPlantAndPest> dao;

    @Override
    public PreparationForPlantAndPest find(Object primaryKey) {
        return dao.getOne(primaryKey);
    }

    @Override
    public List<PreparationForPlantAndPest> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void save(PreparationForPlantAndPest preparationForPlantAndPest) {
        dao.save(preparationForPlantAndPest);
    }

    @Override
    public void update(PreparationForPlantAndPest preparationForPlantAndPest) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(PreparationForPlantAndPest preparationForPlantAndPest) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setDao(GenericDAO<PreparationForPlantAndPest> dao) {
        this.dao = dao;
    }
}
