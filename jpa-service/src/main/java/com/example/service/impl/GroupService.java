package com.example.service.impl;

import com.example.model.students.Group;
import com.example.service.GenericDAO;
import com.example.service.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 17.06.13
 * Time: 16:00
 */
public class GroupService implements Service<Group> {

    private GenericDAO<Group> dao;

    @Override
    public Group find(Object primaryKey) {
        return dao.getOne(primaryKey);
    }

    @Override
    public List<Group> findAll() {
        return null;
    }

    @Override
    public void save(Group group) {
        dao.save(group);
    }

    @Override
    public void update(Group group) {
        dao.update(group);
    }

    @Override
    public void delete(Group group) {
        dao.delete(group);
    }

    @Override
    public void setDao(GenericDAO<Group> dao) {
        this.dao = dao;
    }
}
