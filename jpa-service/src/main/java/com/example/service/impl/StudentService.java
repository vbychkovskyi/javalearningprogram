package com.example.service.impl;

import com.example.model.students.Student;
import com.example.service.GenericDAO;
import com.example.service.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 17.06.13
 * Time: 15:57
 */

public class StudentService implements Service<Student> {

    private GenericDAO<Student> studentGenericDAO;

    @Override
    public Student find(Object primaryKey) {
        return studentGenericDAO.getOne(primaryKey);
    }

    @Override
    public List<Student> findAll() {
        return null;//studentGenericDAO.getAll();
    }

    @Override
    public void save(Student student) {
        studentGenericDAO.save(student);
    }

    @Override
    public void update(Student student) {
        studentGenericDAO.update(student);// emf.merge(student);
    }

    @Override
    public void delete(Student student) {
        studentGenericDAO.delete(student);
    }

    @Override
    public void setDao(GenericDAO<Student> dao) {
        this.studentGenericDAO = dao;
    }
}
