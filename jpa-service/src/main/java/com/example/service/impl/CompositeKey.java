package com.example.service.impl;

import com.example.model.plants.classId.TableEntity;
import com.example.service.GenericDAO;
import com.example.service.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 20.06.13
 * Time: 17:17
 */
@Transactional
public class CompositeKey implements Service<TableEntity> {

    private GenericDAO<TableEntity> dao;

    @Override
    public TableEntity find(Object primaryKey) {
        return dao.getOne(primaryKey);
    }

    @Override
    public List<TableEntity> findAll() {
        return null;
    }

    @Override
    public void save(TableEntity tableEntity) {
        dao.save(tableEntity);
    }

    @Override
    public void update(TableEntity tableEntity) {

    }

    @Override
    public void delete(TableEntity tableEntity) {

    }

    @Override
    public void setDao(GenericDAO<TableEntity> dao) {
        this.dao = dao;
    }
}
