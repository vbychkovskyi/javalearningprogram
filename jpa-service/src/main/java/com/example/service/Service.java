package com.example.service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 17.06.13
 * Time: 14:12
 */

@Transactional
public interface Service<T> {
    T find(Object primaryKey);

    List<T> findAll();

    void save(T t);

    void update(T t);

    void delete(T t);

    void setDao(GenericDAO<T> dao);
}
