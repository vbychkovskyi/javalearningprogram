package com.example.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 18.06.13
 * Time: 11:31
 */
public class GenericDAOImpl<T> implements GenericDAO<T> {

    @PersistenceContext
    public EntityManager entityManager;

    Class<T> type;

    public GenericDAOImpl() {
    }

    public GenericDAOImpl(Class<T> clazz) {
        this.type = clazz;
    }

    @Override
    public T getOne(Object primaryKey) {
        return entityManager.find(type, primaryKey);
    }

    @Override
    public void save(T t) {
        entityManager.persist(t);
    }

    @Override
    public T delete(T t) {
        entityManager.remove(entityManager.merge(t));
        return t;
    }

    @Override
    public void update(T t) {
        entityManager.merge(t);
    }


}
