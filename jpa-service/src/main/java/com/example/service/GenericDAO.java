package com.example.service;

/**
 * Created with IntelliJ IDEA.
 * User: Vyacheslav.Bychkovsk
 * Date: 18.06.13
 * Time: 11:25
 */

public interface GenericDAO<T> {

    T getOne(Object primaryKey);

    void save(T t);

    T delete(T t);

    void update(T t);

}
